#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/dnn.hpp>
#include <opencv2/dnn/shape_utils.hpp>
#include <image_transport/image_transport.h>

#include <ros/ros.h>
#include <signal.h>
#include <termios.h>
#include <stdio.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CompressedImage.h>
#include <std_msgs/String.h>

#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>

#include "face_detection.h"
#include "face_alignment.h"
#include "face_identification.h"

using namespace cv;
using namespace cv::dnn;
using namespace std;

const char* classNames[] = {"background",
                            "aeroplane", "bicycle", "bird", "boat",
                            "bottle", "bus", "car", "cat", "chair",
                            "cow", "diningtable", "dog", "horse",
                            "motorbike", "person", "pottedplant",
                            "sheep", "sofa", "train", "tvmonitor"};

class DetectorMobileNet
{
public:
    DetectorMobileNet(ros::NodeHandle& nh);
    ~DetectorMobileNet(){cv::destroyWindow("person_detector");}
    void start();

private:
    ros::NodeHandle& nh_;
    image_transport::ImageTransport imageTransport_;
    image_transport::TransportHints hints_;
    image_transport::Subscriber image_sub_;
    int64 work_begin_;
    double work_fps_;

    ros::Publisher voicePublisher_;

    const size_t frameWidth=640;
    const size_t frameHeight = 480;
    const size_t inWidth = 300;
    const size_t inHeight = 300;
    const float WHRatio = inWidth / (float)inHeight;
    const float inScaleFactor = 0.007843f;
    const float meanVal = 127.5;
    Rect crop_;
    bool if1Called_,if2Called_;
    boost::thread call1TimerThread_,call2TimerThread_;


    boost::mutex image_mutex_;
    cv::dnn::Net net_;
    cv::Mat image_;

    void workBegin();
    void workEnd();

    void faceCheckLoop();

    void imageCb(const sensor_msgs::ImageConstPtr& msg);
    void getImage(cv::Mat &image);
    void sendMsgToVoice(bool if1Found, bool if2Found);
    void calledTimeOut(bool& ifCalled);
};



DetectorMobileNet::DetectorMobileNet(ros::NodeHandle& nh):nh_(nh),imageTransport_(nh),hints_("compressed"),if1Called_(false)
{
  voicePublisher_ = nh_.advertise<std_msgs::String>("play_voice",1000,true);

  cv::namedWindow("person_detector");
  net_= readNetFromCaffe("/home/jimmy/code/data/MobileNetSSD_deploy.prototxt","/home/jimmy/code/data/MobileNetSSD_deploy.caffemodel");
  image_sub_ =  imageTransport_.subscribe( "/usb_cam/image_raw", 10, &DetectorMobileNet::imageCb ,this,hints_);

  Size cropSize;
  if (frameWidth / (float)frameHeight > WHRatio)
  {
      cropSize = Size(static_cast<int>(frameHeight * WHRatio),
                      frameHeight);
  }
  else
  {
      cropSize = Size(frameWidth,
                      static_cast<int>(frameWidth / WHRatio));
  }
  crop_ = Rect(Point((frameWidth - cropSize.width) / 2,
                     (frameHeight - cropSize.height) / 2),
               cropSize);
}

void DetectorMobileNet::start(){

    boost::thread faceThread(boost::bind(&DetectorMobileNet::faceCheckLoop,this));


    while(true){
       workBegin();
       cv::Mat src;
       getImage(src);
       if(src.cols ==0){
           workEnd();
           continue;
       }
       Mat inputBlob = blobFromImage(src, inScaleFactor,
                                     Size(inWidth, inHeight), meanVal, false);

       net_.setInput(inputBlob, "data"); //set the network input

       Mat detection = net_.forward("detection_out"); //compute output
       //! [Make forward pass]

       Mat detectionMat(detection.size[2], detection.size[3], CV_32F, detection.ptr<float>());

       src = src(crop_);

       float confidenceThreshold = 0.3;
       for(int i = 0; i < detectionMat.rows; i++)
       {
           float confidence = detectionMat.at<float>(i, 2);


           if(confidence > confidenceThreshold)
           {
               size_t objectClass = (size_t)(detectionMat.at<float>(i, 1));


               if (String(classNames[objectClass])!="person"){
                   continue;
               }
               int xLeftBottom = static_cast<int>(detectionMat.at<float>(i, 3) * src.cols);
               int yLeftBottom = static_cast<int>(detectionMat.at<float>(i, 4) * src.rows);
               int xRightTop = static_cast<int>(detectionMat.at<float>(i, 5) * src.cols);
               int yRightTop = static_cast<int>(detectionMat.at<float>(i, 6) * src.rows);

               ostringstream ss;
               ss << confidence;
               String conf(ss.str());

               Rect object((int)xLeftBottom, (int)yLeftBottom,
                           (int)(xRightTop - xLeftBottom),
                           (int)(yRightTop - yLeftBottom));

               rectangle(src, object, Scalar(0, 255, 0));
               String label = String(classNames[objectClass]) + ": " + conf;
               int baseLine = 0;

               Size labelSize = getTextSize(label, FONT_HERSHEY_SIMPLEX, 0.5, 1, &baseLine);
               rectangle(src, Rect(Point(xLeftBottom, yLeftBottom - labelSize.height),
                                     Size(labelSize.width, labelSize.height + baseLine)),
                         Scalar(255, 255, 255), CV_FILLED);
               putText(src, label, Point(xLeftBottom, yLeftBottom),
                       FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0,0,0));
           }
       }
       workEnd();

       cv::putText(src, "mobilenet detector", cv::Point(5, 25), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(255, 0, 0), 2);
       std::ostringstream fpsText;
       fpsText << "FPS: " << work_fps_;
       cv::putText(src, fpsText.str(), cv::Point(5, 55), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0, 255, 0), 2);

       cv::imshow("person_detector",src);
       cv::waitKey(3);
    }

    faceThread.join();

}

void DetectorMobileNet::getImage(cv::Mat &image) {
    boost::unique_lock<boost::mutex> lk( image_mutex_ );
    image = image_.clone();
    lk.unlock();
}

void DetectorMobileNet::sendMsgToVoice(bool if1Found,bool if2Found)
{

    if((if1Found&&if2Found)&&!(if1Called_&&if2Called_))
    {
        std_msgs::String msg;
        msg.data = "3";
        voicePublisher_.publish(msg);

        cout<<"Hello 1 and 2"<<std::endl;
        if(!if1Called_)
            call1TimerThread_ = boost::thread(boost::bind(&DetectorMobileNet::calledTimeOut,this,boost::ref(if1Called_)));
        if(!if2Called_)
            call2TimerThread_ = boost::thread(boost::bind(&DetectorMobileNet::calledTimeOut,this,boost::ref(if2Called_)));

        return;

    }
    if(if1Found&&!if1Called_) {

        std_msgs::String msg;
        msg.data = "1";
        voicePublisher_.publish(msg);

        cout<<"Hello 1"<<std::endl;
        call1TimerThread_ = boost::thread(boost::bind(&DetectorMobileNet::calledTimeOut,this,boost::ref(if1Called_)));
        return;
    }

    if(if2Found&&!if2Called_) {
        std_msgs::String msg;
        msg.data = "2";
        voicePublisher_.publish(msg);

        cout<<"Hello 2"<<std::endl;
        call2TimerThread_ = boost::thread(boost::bind(&DetectorMobileNet::calledTimeOut,this,boost::ref(if2Called_)));
        return;
    }
}

void DetectorMobileNet::calledTimeOut(bool &ifCalled)
{
    ifCalled = true;
    boost::this_thread::sleep(boost::posix_time::seconds(30));
    ifCalled = false;
    cout<<"timer thread dead"<<endl;
}

void DetectorMobileNet::workBegin()
{
    work_begin_ = cv::getTickCount();
}

void DetectorMobileNet::workEnd()
{
    int64 delta = cv::getTickCount() - work_begin_;
    double freq = cv::getTickFrequency();
    work_fps_ = freq / delta;
}

void DetectorMobileNet::faceCheckLoop()
{

    cv::Mat img,img_gray;
    seeta::ImageData img_data;
    img_data.num_channels = 1;


    seeta::FaceAlignment point_detector("/home/jimmy/code/data/seeta_fa_v1.1.bin");
    seeta::FaceIdentification face_recognizer("/home/jimmy/code/data/seeta_fr_v1.0.bin");
    seeta::FaceDetection detector("/home/jimmy/code/data/seeta_fd_frontal_v1.0.bin");
    detector.SetMinFaceSize(40);
    detector.SetScoreThresh(2.f);
    detector.SetImagePyramidScaleFactor(0.8f);
    detector.SetWindowStep(4, 4);

    float gallery_fea1[2048];
    float gallery_fea2[2048];
    float probe_fea[2048];

    ifstream fin;
    fin.open("/home/jimmy/code/data/1.feat", std::ios::binary);
    fin.read((char*)gallery_fea1,sizeof(float)*2048);
    fin.close();

    fin.open("/home/jimmy/code/data/2.feat", std::ios::binary);
    fin.read((char*)gallery_fea2,sizeof(float)*2048);
    fin.close();

    while(true) {
        boost::this_thread::sleep(boost::posix_time::seconds(2));
        getImage(img);
        if(img.cols ==0)
            continue;

        if (img.channels() != 1)
            cv::cvtColor(img, img_gray, cv::COLOR_BGR2GRAY);
        else
            img_gray = img;

        img_data.data = img_gray.data;
        img_data.width = img_gray.cols;
        img_data.height = img_gray.rows;
        workBegin();

        std::vector<seeta::FaceInfo> faces = detector.Detect(img_data);

        cv::Rect face_rect;
        int32_t num_face = static_cast<int32_t>(faces.size());

        seeta::FacialLandmark points[5];
        bool if1Found =false;
        bool if2Found =false;

        for (int32_t i = 0; i < num_face; i++) {
            face_rect.x = faces[i].bbox.x;
            face_rect.y = faces[i].bbox.y;
            face_rect.width = faces[i].bbox.width;
            face_rect.height = faces[i].bbox.height;

            point_detector.PointDetectLandmarks(img_data, faces[i], points);

            seeta::ImageData img_data_color(img.cols,img.rows,img.channels());
            img_data_color.data = img.data;

            face_recognizer.ExtractFeatureWithCrop(img_data_color, points, probe_fea);
            float sim = face_recognizer.CalcSimilarity(gallery_fea1, probe_fea);

            if(sim>0.65) {
                if1Found = true;
//                cout<<"1 found"<<endl;
            }

            sim = face_recognizer.CalcSimilarity(gallery_fea2, probe_fea);

            if(sim>0.65) {
                if2Found = true;
//                cout<<"2 found"<<endl;
            }

        }

        if(if1Found||if2Found) {
           sendMsgToVoice(if1Found,if2Found);
        }

    }
}

void DetectorMobileNet::imageCb(const sensor_msgs::ImageConstPtr &msg)
{
    cv_bridge::CvImageConstPtr cv_ptr;

    try {
        cv_ptr = cv_bridge::toCvShare(msg, sensor_msgs::image_encodings::BGR8);
    } catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bdrige exception: %s", e.what());
        return;
    }


    boost::unique_lock<boost::mutex> lk( image_mutex_ );
    image_=cv_ptr->image.clone();
    lk.unlock();

}

void spin() {
    ros::spin();
}

int main(int argc, char *argv[])
{

    ros::init(argc, argv,"personDetect");
    ros::NodeHandle nh;

    DetectorMobileNet detector(nh);
    boost::thread spinTread(&spin);
    detector.start();

    spinTread.join();

    return 0;
}


